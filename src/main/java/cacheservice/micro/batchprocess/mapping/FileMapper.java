package cacheservice.micro.batchprocess.mapping;

import cacheservice.micro.model.CacheDto;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;
/*
* Fields of uploaded csv file is mapped to java pojo according the field name
* */
public class FileMapper implements FieldSetMapper<CacheDto> {
    @Override
    public CacheDto mapFieldSet(FieldSet fieldSet) throws BindException {
        CacheDto cacheDto = new CacheDto();
        try {
            cacheDto.setKey(fieldSet.readString("key"));
            cacheDto.setValue(fieldSet.readString("value"));
            /*
            * Default expire time is 10
            * when user provide expire time less than 1 second
            * then, expire time set to default
            * otherwise, expire time set to user given second
            * */
            if (fieldSet.readInt("expiretime") < 1) {
                cacheDto.setExpireTime(10);
            }
            else {
                cacheDto.setExpireTime(fieldSet.readInt("expiretime"));
            }
        } catch (Exception e) {
            cacheDto.setExpireTime(10);
        }
        return cacheDto;
    }
}
