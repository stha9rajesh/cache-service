package cacheservice.micro.batchprocess.writer;


import cacheservice.micro.entity.CacheEntity;
import cacheservice.micro.repository.CacheRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CacheDbWritter implements ItemWriter<CacheEntity> {
    private final Logger logger = LoggerFactory.getLogger(CacheDbWritter.class);
    private CacheRepository cacheRepository;

    @Autowired
    public CacheDbWritter(CacheRepository cacheRepository) {
        this.cacheRepository = cacheRepository;
    }

    @Override
    public void write(List<? extends CacheEntity> list) throws Exception {
         cacheRepository.saveAll(list);
    }
}
