package cacheservice.micro.batchprocess.job;

import cacheservice.micro.batchprocess.writer.CacheDbWritter;
import cacheservice.micro.batchprocess.processor.CacheProcessor;
import cacheservice.micro.batchprocess.mapping.FileMapper;
import cacheservice.micro.model.CacheDto;
import cacheservice.micro.entity.CacheEntity;
import cacheservice.micro.repository.CacheRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;


@Configuration
public class CacheJob {

    private JobBuilderFactory jobBuilderFactory;
    private StepBuilderFactory stepBuilderFactory;
    private CacheRepository cacheRepository;
    private CacheProcessor cacheProcessor;
    private CacheDbWritter cacheDbWritter;


    @Autowired
    public CacheJob(JobBuilderFactory jobBuilderFactory,
                    StepBuilderFactory stepBuilderFactory,
                    CacheRepository cacheRepository,
                    CacheProcessor cacheProcessor,
                    CacheDbWritter cacheDbWritter) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.cacheRepository = cacheRepository;
        this.cacheProcessor = cacheProcessor;
        this.cacheDbWritter = cacheDbWritter;
    }

    @Bean
    @StepScope
    Resource inputFileResource(@Value("#{jobParameters[fullPathFileName]}") String pathToFile) {
        return new FileSystemResource(pathToFile);
    }
/*
* reads csv file contents from uploaded file
* */
    @Bean
    @Scope
    public FlatFileItemReader<CacheDto> dataReader() {
        FlatFileItemReader<CacheDto> reader = new FlatFileItemReader<>();
        reader.setLinesToSkip(1);
        reader.setResource(inputFileResource(null));

        lineMapper(reader);
        return reader;
    }
/*
* maps csv file's field name to java pojo i.e CacheDto
* */
    private void lineMapper(FlatFileItemReader<CacheDto> reader) {
        DefaultLineMapper<CacheDto> defaultLineMapper = new DefaultLineMapper<>();

        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setNames(new String[]{"key", "value","expiretime"});

        defaultLineMapper.setLineTokenizer(tokenizer);
        defaultLineMapper.setFieldSetMapper(new FileMapper());
        defaultLineMapper.afterPropertiesSet();

        reader.setLineMapper(defaultLineMapper);
    }


    @Bean
    public Step step1() {
        return stepBuilderFactory.get("uploadDatatoDb")
                .<CacheDto, CacheEntity>chunk(1500)
                .reader(dataReader())
                .processor(cacheProcessor)
                .writer(cacheDbWritter)
                .build();
    }

    @Bean
    public Job importUserJob() {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();

    }
}
