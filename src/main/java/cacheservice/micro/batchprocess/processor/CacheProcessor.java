package cacheservice.micro.batchprocess.processor;

import cacheservice.micro.model.CacheDto;
import cacheservice.micro.entity.CacheEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
/*
* map Dto to Entity
* */
@Component
public class CacheProcessor implements ItemProcessor<CacheDto, CacheEntity> {
    private final Logger logger = LoggerFactory.getLogger(CacheProcessor.class);

    @Override
    public CacheEntity process(CacheDto cacheDto) throws Exception {
        CacheEntity entity = new CacheEntity();
        entity.setMyValue(cacheDto.getValue());
        entity.setMyKey(cacheDto.getKey());
        /*
        * Add given expire_second by user to current timeAndDate to process for expire_data later on
        * */
        entity.setExpireTime(LocalDateTime.now()
                                .plusSeconds(cacheDto.getExpireTime()));
        return entity;
    }
}
