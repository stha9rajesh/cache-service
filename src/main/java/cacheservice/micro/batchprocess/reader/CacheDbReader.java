package cacheservice.micro.batchprocess.reader;

import cacheservice.micro.entity.CacheEntity;
import org.springframework.batch.item.*;
import org.springframework.stereotype.Component;

@Component
public class CacheDbReader implements ItemStreamReader<CacheEntity> {

    @Override
    public CacheEntity read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        return null;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {

    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {

    }

    @Override
    public void close() throws ItemStreamException {

    }
}
