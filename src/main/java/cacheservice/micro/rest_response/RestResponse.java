package cacheservice.micro.rest_response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestResponse implements Serializable {
    private String message;
    private Object details;

}
