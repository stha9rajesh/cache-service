package cacheservice.micro.repository;

import cacheservice.micro.entity.CacheEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CacheRepository extends JpaRepository<CacheEntity, Integer> {
    Optional<CacheEntity> findByMyKey(String key);
}
