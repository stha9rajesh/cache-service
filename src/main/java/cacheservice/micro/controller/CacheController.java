package cacheservice.micro.controller;

import cacheservice.micro.entity.CacheEntity;
import cacheservice.micro.repository.CacheRepository;
import cacheservice.micro.rest_response.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Optional;

@RestController
public class CacheController {
    private final Logger logger = LoggerFactory.getLogger(CacheController.class);
    private Job importUserJob;
    private JobLauncher jobLauncher;
    private CacheRepository cacheRepository;

    public CacheController(Job importUserJob,
                           Job readCache,
                           JobLauncher jobLauncher,
                           CacheRepository cacheRepository) {
        this.importUserJob = importUserJob;
        this.jobLauncher = jobLauncher;
        this.cacheRepository = cacheRepository;
    }
/*
* upload csv file for batch process
* files will be uploaded in 'uploads' directory located in classpath
* */
    @PostMapping("/upload")
    public String uploadFile(@Value("file") MultipartFile file) {
        String result = null;
        try {
            if (file != null) {
                Path uploadDir = Paths.get("uploads");
                Path filePath = uploadDir.resolve(file.getOriginalFilename());

                if (Files.notExists(uploadDir)) {
                    Files.createDirectory(uploadDir);
                }

                if (Files.notExists(filePath)) {
                    Files.copy(file.getInputStream(), filePath);
                }

                //Launch the Batch Job
                JobExecution jobExecution = jobLauncher.run(importUserJob, new JobParametersBuilder()
                        .addString("fullPathFileName", filePath.toAbsolutePath().toString())
                        .toJobParameters());

                return "done";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @GetMapping("cache/{id}")
    public ResponseEntity<RestResponse> cacheEntity(@PathVariable("id") int id) {
        RestResponse restResponse = null;
        try {
            Optional<CacheEntity> cacheEntity = cacheRepository.findById(id);
            if (cacheEntity.isPresent()) {
                if (LocalDateTime.now().isAfter(cacheEntity.get().getExpireTime())) {
                    deleteCache(cacheEntity.get());
                    return new ResponseEntity<RestResponse>(new RestResponse("Data Expired", null), HttpStatus.NOT_FOUND);
                }
                restResponse = new RestResponse("Data Found", cacheEntity.get());
            } else {
                restResponse = new RestResponse("Data Not Found!", null);
            }
        } catch (Exception e) {
            restResponse = new RestResponse("Internal Server Error", null);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(restResponse);
    }

    private void deleteCache(CacheEntity cacheEntity) {
        cacheRepository.delete(cacheEntity);
    }

    @GetMapping("cache/{key}/key")
    public ResponseEntity<RestResponse> cacheEntity(@PathVariable("key") String key) {
        RestResponse restResponse = null;
        try {
            System.out.println(LocalDateTime.now());
            Optional<CacheEntity> cacheEntity = cacheRepository.findByMyKey(key);
            if (cacheEntity.isPresent()) {
                if (LocalDateTime.now().isAfter(cacheEntity.get().getExpireTime())) {
                    deleteCache(cacheEntity.get());
                    return new ResponseEntity<RestResponse>(new RestResponse("Data Expired", null),
                            HttpStatus.NOT_FOUND);
                }
                restResponse = new RestResponse("Data Found", cacheEntity.get());
            } else {
                restResponse = new RestResponse("Data Not Found!", null);
            }
        } catch (Exception e) {
            restResponse = new RestResponse("Internal Server Error", null);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(restResponse);
    }


}
