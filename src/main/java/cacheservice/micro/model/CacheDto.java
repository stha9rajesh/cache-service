package cacheservice.micro.model;

public class CacheDto {
    private String key;
    private String value;
    private int expireTime;

    public CacheDto() {
    }

    public CacheDto(String key, String value, int expireTime) {
        this.key = key;
        this.value = value;
        this.expireTime = expireTime;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(int expireTime) {
        this.expireTime = expireTime;
    }
}
